# Somobu's private F-Droid repo

## How to add this repo

F-Droid -> Settings -> Repositories -> "+":
```
Repository address: https://somobu.gitlab.io/f-repo
Fingerprint (optional): CEF615744B62A8E1638AF0D3608FEB062879F7D0E8BD1A8B1C42F29D0AA87957
```


## How f-droid repo is built

`head.json` file is meant to keep repository info (`repo` section of resulting `index-v1.json`). For now it is copied to index as-is.

`apps` and `packages` sections are generated from `apps.json` file. This file contains array of json objects, each stores per-app data. All keys (except listed below) from each json object will be copied as-is into corresponding object in resulting `apps` section.

Which keys will not be copied into resulting app info:

| Key | Purpose |
| --- | --- |
| metaBaseUrl | Describes project's GitLab url |
| minSdkVersion | Package info |
| targetSdkVersion | Package info |
| uses-permission | Package info |
| apkName | Package info -- how app will be named in this repo |
| apkUrl | Package info -- link from which release apk will be downloaded|
| signatureMd5 | Package info -- signing key md5 hash |


Which app info keys will be auto-generated:

| Key | Source |
| --- | --- |
| lastUpdated | Current time |
| sourceCode | `metaBaseUrl` |
| changelog | `metaBaseUrl` + "/-/tags" |
| issueTracker | `metaBaseUrl` + "/-/issues"

